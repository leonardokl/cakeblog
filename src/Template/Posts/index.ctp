<div class="actions columns large-2 medium-3">
    
    <h3><?= __('PROINT') ?></h3>
    <?php 
        if($this->request->session()->read('Auth.User')){
    ?>
    <ul class="side-nav">
        <li><?= $this->Html->link(__('New Post'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?></li>
    </ul>
    <?php 
        }
    ?>
</div>
<div class="posts index large-10 medium-9 columns">
    
    <?php foreach ($posts as $post): ?>
    <div>
        <tr>           
            <td><h1><?= $this->Html->link($post->title, ['action' => 'view', $post->id]) ?></h1></td>
            <td>
                Por <?= $post->has('user') ? $this->Html->link($post->user->name, ['controller' => 'Users', 'action' => 'view', $post->user->id]) : '' ?>
            </td>

            <?php 
                if($this->request->session()->read('Auth.User')){
            ?>
            <td class="actions"><span style="float:right">                
                <?= $this->Html->link(__('Edit'), ['action' => 'edit', $post->id]) ?>
                <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $post->id], ['confirm' => __('Are you sure you want to delete # {0}?', $post->id)]) ?></span>
            </td>
            <?php 
                }
            ?>
            
            <hr>
        </tr>
    </div>
    <?php endforeach; ?>
   
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
