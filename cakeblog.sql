-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 18-Jun-2015 às 06:22
-- Versão do servidor: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `cakeblog`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `posts`
--

CREATE TABLE IF NOT EXISTS `posts` (
`id` int(12) NOT NULL,
  `title` varchar(255) NOT NULL,
  `body` text NOT NULL,
  `user_id` int(12) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `posts`
--

INSERT INTO `posts` (`id`, `title`, `body`, `user_id`) VALUES
(3, 'Lorem ipsum', 'Lorem ipsum dolor sit amet, ex eam facer deserunt volutpat, pro no facete numquam, sea persecuti intellegebat id. Pri erant apeirian verterem ex, est te platonem mandamus volutpat. Utinam iracundia deterruisset ad duo, pri error soleat ei, sea cu legere nostro saperet. An dictas atomorum has, te primis nominavi cum, at mea omnis conceptam. Appetere accusamus euripidis eam no.\r\n\r\nAd vel autem delenit interpretaris, illum ludus ei sit. No facer aperiri nam, ius volumus noluisse id, graece primis cu duo. Ea amet mediocrem percipitur eum. Labore philosophia id duo. Ne nec dictas meliore vituperatoribus.\r\n\r\nMelius habemus suscipiantur ne mea, zril iisque ceteros an cum, odio feugiat ne vis. Ex per suas referrentur, his aliquid legendos splendide ad. Euismod sanctus ea eam, viris recusabo per an. Sit at alienum definiebas consequuntur, labore molestie in vim, euismod accusam persecuti et his. His ea blandit appetere. Sumo tractatos ei vim, at novum nullam gloriatur quo, te vis dicit elaboraret comprehensam.\r\n\r\nMeis clita pro id, ut magna porro vim. Detraxit legendos in mel, ex movet molestie vix. Causae accusam pericula vim ad. Laoreet facilis senserit cu vis, ut qui viris impedit mediocritatem, mundi erroribus reprimique vim cu.\r\n\r\nAn affert laboramus his. Quo ex nibh propriae, eum in euismod consetetur, at epicuri adipiscing his. Eos ei nostrum consequat, scribentur ullamcorper et cum. Duo nullam dolores qualisque id, eos phaedrum iracundia et. Iriure scripta salutandi eum in, mea eius errem ponderum id.', 2),
(4, 'Teste', 'Lorem ipsum dolor sit amet, ex eam facer deserunt volutpat, pro no facete numquam, sea persecuti intellegebat id. Pri erant apeirian verterem ex, est te platonem mandamus volutpat. Utinam iracundia deterruisset ad duo, pri error soleat ei, sea cu legere nostro saperet. An dictas atomorum has, te primis nominavi cum, at mea omnis conceptam. Appetere accusamus euripidis eam no.\r\n\r\nAd vel autem delenit interpretaris, illum ludus ei sit. No facer aperiri nam, ius volumus noluisse id, graece primis cu duo. Ea amet mediocrem percipitur eum. Labore philosophia id duo. Ne nec dictas meliore vituperatoribus.\r\n\r\nMelius habemus suscipiantur ne mea, zril iisque ceteros an cum, odio feugiat ne vis. Ex per suas referrentur, his aliquid legendos splendide ad. Euismod sanctus ea eam, viris recusabo per an. Sit at alienum definiebas consequuntur, labore molestie in vim, euismod accusam persecuti et his. His ea blandit appetere. Sumo tractatos ei vim, at novum nullam gloriatur quo, te vis dicit elaboraret comprehensam.\r\n\r\nMeis clita pro id, ut magna porro vim. Detraxit legendos in mel, ex movet molestie vix. Causae accusam pericula vim ad. Laoreet facilis senserit cu vis, ut qui viris impedit mediocritatem, mundi erroribus reprimique vim cu.\r\n\r\nAn affert laboramus his. Quo ex nibh propriae, eum in euismod consetetur, at epicuri adipiscing his. Eos ei nostrum consequat, scribentur ullamcorper et cum. Duo nullam dolores qualisque id, eos phaedrum iracundia et. Iriure scripta salutandi eum in, mea eius errem ponderum id.', 2),
(5, 'Mais um post', 'Lorem ipsum dolor sit amet, ex eam facer deserunt volutpat, pro no facete numquam, sea persecuti intellegebat id. Pri erant apeirian verterem ex, est te platonem mandamus volutpat. Utinam iracundia deterruisset ad duo, pri error soleat ei, sea cu legere nostro saperet. An dictas atomorum has, te primis nominavi cum, at mea omnis conceptam. Appetere accusamus euripidis eam no.\r\n\r\nAd vel autem delenit interpretaris, illum ludus ei sit. No facer aperiri nam, ius volumus noluisse id, graece primis cu duo. Ea amet mediocrem percipitur eum. Labore philosophia id duo. Ne nec dictas meliore vituperatoribus.\r\n\r\nMelius habemus suscipiantur ne mea, zril iisque ceteros an cum, odio feugiat ne vis. Ex per suas referrentur, his aliquid legendos splendide ad. Euismod sanctus ea eam, viris recusabo per an. Sit at alienum definiebas consequuntur, labore molestie in vim, euismod accusam persecuti et his. His ea blandit appetere. Sumo tractatos ei vim, at novum nullam gloriatur quo, te vis dicit elaboraret comprehensam.\r\n\r\nMeis clita pro id, ut magna porro vim. Detraxit legendos in mel, ex movet molestie vix. Causae accusam pericula vim ad. Laoreet facilis senserit cu vis, ut qui viris impedit mediocritatem, mundi erroribus reprimique vim cu.\r\n\r\nAn affert laboramus his. Quo ex nibh propriae, eum in euismod consetetur, at epicuri adipiscing his. Eos ei nostrum consequat, scribentur ullamcorper et cum. Duo nullam dolores qualisque id, eos phaedrum iracundia et. Iriure scripta salutandi eum in, mea eius errem ponderum id.', 3),
(7, 'Auth', 'Lorem ipsum dolor sit amet, ex eam facer deserunt volutpat, pro no facete numquam, sea persecuti intellegebat id. Pri erant apeirian verterem ex, est te platonem mandamus volutpat. Utinam iracundia deterruisset ad duo, pri error soleat ei, sea cu legere nostro saperet. An dictas atomorum has, te primis nominavi cum, at mea omnis conceptam. Appetere accusamus euripidis eam no.\r\n\r\nAd vel autem delenit interpretaris, illum ludus ei sit. No facer aperiri nam, ius volumus noluisse id, graece primis cu duo. Ea amet mediocrem percipitur eum. Labore philosophia id duo. Ne nec dictas meliore vituperatoribus.\r\n\r\nMelius habemus suscipiantur ne mea, zril iisque ceteros an cum, odio feugiat ne vis. Ex per suas referrentur, his aliquid legendos splendide ad. Euismod sanctus ea eam, viris recusabo per an. Sit at alienum definiebas consequuntur, labore molestie in vim, euismod accusam persecuti et his. His ea blandit appetere. Sumo tractatos ei vim, at novum nullam gloriatur quo, te vis dicit elaboraret comprehensam.\r\n\r\nMeis clita pro id, ut magna porro vim. Detraxit legendos in mel, ex movet molestie vix. Causae accusam pericula vim ad. Laoreet facilis senserit cu vis, ut qui viris impedit mediocritatem, mundi erroribus reprimique vim cu.\r\n\r\nAn affert laboramus his. Quo ex nibh propriae, eum in euismod consetetur, at epicuri adipiscing his. Eos ei nostrum consequat, scribentur ullamcorper et cum. Duo nullam dolores qualisque id, eos phaedrum iracundia et. Iriure scripta salutandi eum in, mea eius errem ponderum id.', 2),
(8, 'Meis clita pro id', 'Lorem ipsum dolor sit amet, ex eam facer deserunt volutpat, pro no facete numquam, sea persecuti intellegebat id. Pri erant apeirian verterem ex, est te platonem mandamus volutpat. Utinam iracundia deterruisset ad duo, pri error soleat ei, sea cu legere nostro saperet. An dictas atomorum has, te primis nominavi cum, at mea omnis conceptam. Appetere accusamus euripidis eam no.\r\n\r\nAd vel autem delenit interpretaris, illum ludus ei sit. No facer aperiri nam, ius volumus noluisse id, graece primis cu duo. Ea amet mediocrem percipitur eum. Labore philosophia id duo. Ne nec dictas meliore vituperatoribus.\r\n\r\nMelius habemus suscipiantur ne mea, zril iisque ceteros an cum, odio feugiat ne vis. Ex per suas referrentur, his aliquid legendos splendide ad. Euismod sanctus ea eam, viris recusabo per an. Sit at alienum definiebas consequuntur, labore molestie in vim, euismod accusam persecuti et his. His ea blandit appetere. Sumo tractatos ei vim, at novum nullam gloriatur quo, te vis dicit elaboraret comprehensam.\r\n\r\nMeis clita pro id, ut magna porro vim. Detraxit legendos in mel, ex movet molestie vix. Causae accusam pericula vim ad. Laoreet facilis senserit cu vis, ut qui viris impedit mediocritatem, mundi erroribus reprimique vim cu.\r\n\r\nAn affert laboramus his. Quo ex nibh propriae, eum in euismod consetetur, at epicuri adipiscing his. Eos ei nostrum consequat, scribentur ullamcorper et cum. Duo nullam dolores qualisque id, eos phaedrum iracundia et. Iriure scripta salutandi eum in, mea eius errem ponderum id.', 3);

-- --------------------------------------------------------

--
-- Estrutura da tabela `users`
--

CREATE TABLE IF NOT EXISTS `users` (
`id` int(12) NOT NULL,
  `email` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `role` varchar(50) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `users`
--

INSERT INTO `users` (`id`, `email`, `name`, `password`, `role`) VALUES
(2, 'leonardokl@hotmail.com', 'Leonardo Luiz', '$2y$10$JovNCR3s.MZGOhJlLwdvYuyBNsOMKgqFxDjKr3trI5Wxu.5aOFXmK', 'admin'),
(3, 'jessica@email.com', 'Jessica', '$2y$10$V4C7UIJ87E12gOHZOGcUweNEMrIsbKBqQyWo378MASH2mmHa4Fzpm', 'user');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
 ADD PRIMARY KEY (`id`), ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `email` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
MODIFY `id` int(12) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
MODIFY `id` int(12) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- Constraints for dumped tables
--

--
-- Limitadores para a tabela `posts`
--
ALTER TABLE `posts`
ADD CONSTRAINT `posts_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
